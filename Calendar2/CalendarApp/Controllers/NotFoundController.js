﻿app.controller("NotFoundController", function ($scope, $location) {

    $scope.goHome = function () {
        $location.path('/');
    };
    
});