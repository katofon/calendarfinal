﻿app.controller("AuthController", function ($scope, $location, HolidayStorageService, UserEventsStorageService, AuthService, UserEventsService) {
    $scope.name = "";
    $scope.password = "";
    $scope.serverMessages = [];

    $scope.login = function () {
        AuthService.login($scope.name, $scope.password).then(
            function success(token) {
                UserEventsService.getAllUserEvents().then(UserEventsStorageService.recreateObjectStoreEvents);
                HolidayStorageService.clearHolidays();
                $location.path("/week");
            },
            function error(response) {
                $scope.serverMessages.push("Wrong login or password");
            }
        );
    };

    $scope.redirectToRegister = function () {
        $location.path("/register");
    };

    $scope.register = function (valid) {
        if (valid) {
            AuthService.register($scope.name, $scope.password).then(
                function success() {
                    $location.path("/auth");
                },
                function error(response) {
                    $scope.serverMessages.push(response.data.message);
                }
            );
        }
    };
});