﻿app.service("HelperService", function ($routeParams, $location) {
    var LAST_DAY_WEEK_IDX = 6;

    this.getWeekDay = function (date) {
        var day = date.getDay();

        if (day === 0) {
            return LAST_DAY_WEEK_IDX;
        }

        return day - 1;
    };

    this.getKeyByArray = function (arr) {
        return arr.join("_");
    };

    this.getKeyByDate = function (date) {
        return this.getKeyByArray([date.getFullYear(), date.getMonth() + 1, date.getDate()]);
    };

    this.getEventShortText = function (eventParams, maxCharNum, textParamNames) {
        var momentDate = moment(eventParams.dateStart);
        var res = momentDate.format("HH:mm ") + " " + textParamNames.map(function (paramName) { return eventParams[paramName]; }).join(". ");
        return maxCharNum !== null ? res.substr(0, maxCharNum) : res;
    };

    this.uniqueArray = function (array) {
        var newArray = [];
        array.forEach(function (element) {
            if (newArray.indexOf(element) === -1) {
                newArray.push(element);
            }
        });
        return newArray;
    };

    this.getWeekNumberInMonthByDate = function (momentDate, year, month) {
        var firstDateOfMonth = moment([year, month - 1]);
        var firstDateOfWeekInMonth = firstDateOfMonth.startOf('ISOweek');
        return Math.floor(moment.duration(momentDate - firstDateOfWeekInMonth).asWeeks());
    };

    this.switchView = function (viewType) {
        if ($routeParams.year === undefined) {
            $location.path("/" + viewType);
        } else {
            var date = new Date($routeParams.year, $routeParams.month - 1, $routeParams.day);
            $location.path(this.getLinkPathByDate(date, viewType));
        }
    };

    this.getLinkPathByDate = function (date, viewType) {
        return "/" + viewType + "/" + date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    };

    this.getFirstDateOfStartWeekInMonth = function (momentDate) {
        return momentDate.clone().startOf('month').startOf('isoweek');
    };

    this.getLastDateOfLastWeekInMonth = function (momentDate) {
        return momentDate.clone().endOf('month').endOf('isoweek');
    };

    this.lastDayWeekIdx = function () {
        return LAST_DAY_WEEK_IDX;
    };

    this.Boolean = function (val) {
        if (val === null) {
            return false;
        }

        return /^true$/i.test(val.toLowerCase()) === true;
    };

    this.clone = function (item) {
        return JSON.parse(JSON.stringify(item));
    };

    this.sortByParam = function (array, paramName) {
        array.sort(function (first, second) {

            return -first[paramName] + second[paramName];
        });
    };

});