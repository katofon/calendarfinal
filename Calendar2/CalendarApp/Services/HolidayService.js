﻿app.service("HolidayService", function ($q, $http, HolidayStorageService, HelperService) {
    var HOLIDAY_API_KEY = "9d36d077-7be7-4978-a20e-b9f3113aacf6";

    this.getHolidaysByDates = function (momentDateStart, momentDateEnd) {
        var yearsArray = HelperService.uniqueArray([momentDateStart.get('year'), momentDateEnd.get('year')]);
        var downloadYears = yearsArray.filter(function (year) {
            return !HolidayStorageService.hasSavedHolidayEventsByYear(year);
        });

        if (downloadYears.length !== 0) {
            var promises = downloadYears.map(function (year) {
                return $http({
                        url: "https://holidayapi.com/v1/holidays?" + "key=" + HOLIDAY_API_KEY + "&country=RU" + "&year=" + year,
                        method: 'GET'
                    });
            });


            return $q.all(promises).then(function (results) {
                results.forEach(function (result) {
                    var holidaysToSave = [];
                    for (var key in result.data.holidays) {
                        holidaysToSave.push(convertHolidayFromServer(result.data.holidays[key][0]));
                    }
                    HolidayStorageService.addYearHolidays(holidaysToSave, moment(holidaysToSave[0].date).get('year'));
                });
                return HolidayStorageService.getHolidaysByDates(momentDateStart, momentDateEnd);
            });
        } else {
            return HolidayStorageService.getHolidaysByDates(momentDateStart, momentDateEnd);
        }
    };

    function convertHolidayFromServer(holiday) {
        holiday.date = moment(holiday.date).toDate().getTime();
        return holiday;
    }

});