﻿app.service("UserEventsStorageService", function ($q, HelperService) {
    var OBJECT_STORE_NAME = "eventsObjectStore";
    var KEY_PATH = "id";

    function connectEventsDB(f) {
        var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        var request = indexedDB.open("events", 1);

        request.onerror = function (err) {
            console.log(err);
        };

        request.onsuccess = function () {
            f(request.result);
        }

        request.onupgradeneeded = function (e) {
            var objectStore = e.currentTarget.result.createObjectStore(OBJECT_STORE_NAME, { keyPath: KEY_PATH });
            objectStore.createIndex("dateStart", "dateStart", { unique: false });
            connectEventsDB(f);
        }
    }

    this.recreateObjectStoreEvents = function (events) {
        if (events === null)
            return;

        var cloneEvents = HelperService.clone(events);
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);
            objectStore.clear();

            if (cloneEvents.length === 0)
                return;

            cloneEvents.forEach(function (event) {
                objectStore.add(event);
            });
        });
    };

    this.createOrUpdateEvents = function (events) {
        var cloneEvents = HelperService.clone(events);
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);

            cloneEvents.forEach(function (event) {
                objectStore.put(event);
            });
        });
    };

    this.deleteEvent = function (id) {
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);

            objectStore.delete(id);
        });
    };

    this.getEventsByDates = function (momentDateStart, momentDateEnd) {
        var deferred = $q.defer();
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readonly");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);
            var datesRange;

            if (momentDateStart !== undefined) {
                if (momentDateEnd !== undefined) {
                    datesRange = IDBKeyRange.bound(momentDateStart.toDate().getTime(), momentDateEnd.toDate().getTime());
                } else {
                    datesRange = IDBKeyRange.lowerBound(momentDateStart.toDate().getTime());
                }
            } else {
                datesRange = IDBKeyRange.upperBound(momentDateEnd.toDate().getTime());
            }

            var index = objectStore.index("dateStart");

            index.getAll(datesRange).onsuccess = function (eventsResult) {
                deferred.resolve(eventsResult.target.result);
            }
        });

        return deferred.promise;
    };

    this.getEvent = function (id) {
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "read");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);

            return objectStore.get(id);
        });
    };

    this.clearUserEvents = function () {
        connectEventsDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);

            return objectStore.clear();
        });
    };
});







