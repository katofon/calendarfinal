﻿app.service("AuthService", function ($q, $location, $http, $httpParamSerializerJQLike, UserEventsStorageService) {
    this.login = function (name, password) {
        return $http({
            url: "/token",
            method: 'POST',
            data: $httpParamSerializerJQLike({
                "username": name,
                "password": password,
                "grant_type": "password"
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(
            function success(response) {
                setToken(response.data.access_token);
                return response.data.access_token;
            },
            function error(response) {
                var deferred = $q.defer();
                deferred.reject(response);
                return deferred.promise;
            }
        );
    };

    this.register = function (name, password) {
        return $http({
            url: "/api/user/register",
            method: 'POST',
            data: {
                "login": name,
                "password": password,
                "grant_type": "password"
            }
        })
        .then(
            function success(response) {
                return null;
            },

            function error(response) {
                var deferred = $q.defer();
                deferred.reject(response);
                return deferred.promise;
            }
        );
    };

    this.logout = function (name, password) {
        clearToken();
        UserEventsStorageService.clearUserEvents();
        this.goToAuthPage();
    };

    this.getToken = function () {
        return localStorage.getItem("token");
    };

    this.goToAuthPage = function () {
        $location.path("/auth");
    };

    this.checkResponse = function (response) {
        if (response.status === 401) {
            clearToken();
            this.goToAuthPage();
            return false;
        }

        return true;
    };

    function setToken(token) {
        localStorage.setItem("token", token);
    }

    function clearToken() {
        setToken(null);
    }

});