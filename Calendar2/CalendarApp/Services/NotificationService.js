﻿app.service("NotificationService", function ($window, $timeout, $injector, HelperService, UserEventsStorageService) {
    var LOCAL_STORAGE_KEY_NOTIFICATION_STATE = "notificationState";
    var LOCAL_STORAGE_KEY_FOCUSED_WINDOW_ID = "focusedWindow";
    var focusedWindowId;
    var timeoutId;

    this.checkNotifications = function () {
        if (!("Notification" in window)) {
            return;
        }

        if (localStorage.getItem(LOCAL_STORAGE_KEY_NOTIFICATION_STATE) === null) {
            if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    var notificationService = $injector.get('NotificationService');
                    if (permission === "granted") {
                        notificationService.setNotificationState(true);
                        notificationService.checkNotifications();
                    } else {
                        notificationService.setNotificationState(false);
                    }
                });
            }
        } else if (this.getNotificationState()) {
            this.refreshNotification();
            setFocusedWindowId();
            $window.onfocus = function () {
                setFocusedWindowId();
            }
        }

        return this.getNotificationState();
    };

    this.switchNotificationState = function () {
        var state = HelperService.Boolean(localStorage.getItem(LOCAL_STORAGE_KEY_NOTIFICATION_STATE));
        localStorage.setItem(LOCAL_STORAGE_KEY_NOTIFICATION_STATE, !state);
        if (state === false) {
            clearTimeout(timeoutId);
        }
    };

    this.getNotificationState = function () {
        return HelperService.Boolean(localStorage.getItem(LOCAL_STORAGE_KEY_NOTIFICATION_STATE));
    };

    this.setNotificationState = function (state) {
        localStorage.setItem(LOCAL_STORAGE_KEY_NOTIFICATION_STATE, state);
    };

    this.refreshNotification = function () {
        if (!this.getNotificationState())
            return;

        this.updateNotificationTimer();
    };

    this.updateNotificationTimer = function () {
        UserEventsStorageService.getEventsByDates(moment()).then(this.tryRestartTimer);
    };

    this.tryRestartTimer = function (orderedEvents) {
        var timeNow = new Date().getTime();
        var nearestEvent = orderedEvents[0] || null;

        if (nearestEvent === null)
            return;

        timeoutId = setTimeout(function () {
            clearTimeout(timeoutId);
            $injector.get('NotificationService').onTimer({ "text": nearestEvent.name});
        }, nearestEvent.dateStart - timeNow);
    };

    this.onTimer = function (params) {
        if (this.getNotificationState() === false)
            return;

        if (focusedWindowId === localStorage.getItem(LOCAL_STORAGE_KEY_FOCUSED_WINDOW_ID)) {
            new Notification(params.text);
        }

        this.updateNotificationTimer();
    };

    function setFocusedWindowId() {
        var id = new Date().getTime();
        localStorage.setItem(LOCAL_STORAGE_KEY_FOCUSED_WINDOW_ID, id);
        focusedWindowId = String(id);
    }
});