﻿app.directive("headBlock", function ($injector, $location, HelperService, NotificationService, AuthService) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: "/CalendarApp/Views/HeadBlock.html",
        controller: function ($scope) {
            $scope.notificationState = NotificationService.checkNotifications();

            $scope.switchToDayView = function () {
                HelperService.switchView("day");
            };

            $scope.switchToWeekView = function () {
                HelperService.switchView("week");
            };

            $scope.switchToMonthView = function () {
                HelperService.switchView("month");
            };

            $scope.switchToYearView = function () {
                HelperService.switchView("year");
            };

            $scope.switchToYearView = function () {
                HelperService.switchView("year");
            };

            $scope.addEvent = function () {
                $location.path("/event/create");
            };

            $scope.switchNotificationState = function () {
                NotificationService.switchNotificationState();
                $scope.notificationState = NotificationService.getNotificationState();
            };

            $scope.logout = function () {
                AuthService.logout();
            };
        }
    };
});
