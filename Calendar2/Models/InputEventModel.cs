using System;
using System.ComponentModel.DataAnnotations;

namespace Calendar2.Controllers
{
    public class InputEventModel
    {

        [MinLength(3, ErrorMessage = "Min length - 3")]
        [Required(ErrorMessage = "Enter event name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter event start date")]
        public String DateStart { get; set; }

        [Required(ErrorMessage = "Enter event end date")]
        public String DateEnd { get; set; }
        public string Place { get; set; }

        public string Description { get; set; }
        public string OwnerId { get; set; }
        public string Id { get; set; }

    }
}