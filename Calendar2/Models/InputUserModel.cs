using System;
using System.ComponentModel.DataAnnotations;

namespace Calendar2.Controllers
{
    public class InputUserModel
    {
        [MinLength(3, ErrorMessage = "Login is too short")]
        [MaxLength(15, ErrorMessage = "Login is too long")]
        [Required(ErrorMessage = "Login name is required")]
        public String Login { get; set; }

        [MinLength(6, ErrorMessage = "Password is too short")]
        [MaxLength(15, ErrorMessage = "Password is too long")]
        [Required(ErrorMessage = "Password name is required")]
        public String Password { get; set; }

    }
}