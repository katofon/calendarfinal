﻿using System;
using System.Linq;
using Calendar2.Models;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Calendar2.Controllers
{
    [Authorize]
    public class EventController : ApiController
    {

        private readonly ApplicationDbContext context;

        public EventController()
        {
            context = new ApplicationDbContext();
        }

        [HttpGet]
        public IHttpActionResult GetAllUserEvents()
        {
            var uId = User.Identity.GetUserId();
            IQueryable<Event> events_list = (from evt in context.Events
                                             where evt.OwnerId == uId
                                             select evt);

            return Ok(events_list) ;
        }

        [HttpGet]
        public IHttpActionResult GetUserEventsByDates(DateTime dateStart, DateTime dateEnd)
        {
            dateEnd = dateEnd.AddDays(1);
            var uId = User.Identity.GetUserId();
            IQueryable<Event> events_list = (from evt in context.Events
                where evt.OwnerId == uId 
                && evt.DateStart >= dateStart
                && evt.DateStart <= dateEnd
                select evt);

            return Ok(events_list);
        }


        [HttpGet]
        public IHttpActionResult GetUserEvent(int id)
        {
            var uId = User.Identity.GetUserId();
            IQueryable<Event> events_list = (from evt in context.Events
                where evt.Id == id
                select evt);

            return Ok(events_list.First());
        }



        [HttpPost]
        public IHttpActionResult AddNewEvent(InputEventModel inputEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var newEvent = new Event();
            newEvent.Name = inputEvent.Name;
            newEvent.Description = inputEvent.Description;
            newEvent.Place = inputEvent.Place;
            newEvent.DateStart = DateTime.Parse(inputEvent.DateStart);
            newEvent.DateEnd = DateTime.Parse(inputEvent.DateEnd);
            newEvent.OwnerId = User.Identity.GetUserId();

            context.Events.Add(newEvent);
            context.SaveChanges();
            return Ok(newEvent);
        }

        [HttpPost]
        public IHttpActionResult EditEvent(int Id, InputEventModel inputEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IQueryable<Event> events = (from evt in context.Events
                                             where evt.Id == Id
                                             select evt);

            Event dbEvent = events.First();
            dbEvent.Name = inputEvent.Name;
            dbEvent.Description = inputEvent.Description;
            dbEvent.Place = inputEvent.Place;
            dbEvent.DateStart = DateTime.Parse(inputEvent.DateStart);
            dbEvent.DateEnd = DateTime.Parse(inputEvent.DateEnd);

            context.SaveChanges();
            return Ok(dbEvent);
        }

        [HttpDelete]
        public IHttpActionResult DeleteEvent(int id)
        {
            Event eventDelete = context.Events.Find(id);
            if (eventDelete == null)
            {
                return NotFound();
            }
            context.Events.Remove(eventDelete);
            context.SaveChanges();
            return Ok();
        }

        
    }
}