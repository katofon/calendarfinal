﻿using System.Web.Http;
using System.Threading.Tasks;
using Calendar2.Repos;
using Microsoft.AspNet.Identity;

namespace Calendar2.Controllers
{
    public class UserController : ApiController
    {

        private readonly AuthRepository context;

        public UserController()
        {
            context = new AuthRepository();
        }

        public IHttpActionResult Logout()
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody] InputUserModel inputUserModel)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var result = await context.RegisterUser(inputUserModel);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}